-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.12-0ubuntu1 - (Ubuntu)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for set_news
CREATE DATABASE IF NOT EXISTS `set_news` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `set_news`;


-- Dumping structure for table set_news.balance_security
CREATE TABLE IF NOT EXISTS `balance_security` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `level` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `local` varchar(8) NOT NULL DEFAULT '-',
  `foreign` varchar(8) NOT NULL DEFAULT '-',
  `security` varchar(128) NOT NULL,
  `start` varchar(32) NOT NULL,
  `stop` varchar(32) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `local` (`local`),
  KEY `foreign` (`foreign`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table set_news.daily_news
CREATE TABLE IF NOT EXISTS `daily_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL DEFAULT 'untitle',
  `local_date` varchar(32) NOT NULL DEFAULT '0',
  `local_time` varchar(32) NOT NULL DEFAULT '0',
  `symbol` varchar(8) NOT NULL DEFAULT '0',
  `title` text NOT NULL,
  `link` text NOT NULL,
  `owner` varchar(64) NOT NULL,
  `create_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `date_time` (`local_date`),
  KEY `symbol` (`symbol`),
  KEY `local_time` (`local_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table set_news.market_alerts
CREATE TABLE IF NOT EXISTS `market_alerts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL DEFAULT '0',
  `date_time` varchar(32) NOT NULL DEFAULT '0',
  `symbol` varchar(8) NOT NULL DEFAULT '0',
  `title` text NOT NULL,
  `link` text NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `date_time` (`date_time`),
  KEY `symbol` (`symbol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table set_news.research_list
CREATE TABLE IF NOT EXISTS `research_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `local_datetime` varchar(32) NOT NULL,
  `type` varchar(32) NOT NULL,
  `source` varchar(32) NOT NULL,
  `research_type` varchar(32) NOT NULL,
  `security` varchar(32) NOT NULL DEFAULT '-',
  `title` text NOT NULL,
  `link` text NOT NULL,
  `create_at` text NOT NULL,
  `update_at` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `local_datetime` (`local_datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
