/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbmodel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author superx
 */
public class DBConnection {

    private Connection conn;
    public Statement stmt;
    public ResultSet rs;
    private long start;
    private long stop;

    private String url = "jdbc:mysql://localhost/bidschart.com?characterEncoding=UTF8";
    private String driver = "org.gjt.mm.mysql.Driver";
    private String username = "root";
    private String passwd = "1234";
    private String query = "";

    public void connectDB() {
        try {
            Class.forName(getDriver());
            this.setConn(DriverManager.getConnection(getUrl(), getUsername(), getPasswd()));
            //conn.setAutoCommit(false); 
            this.setStmt(getConn().createStatement());
            stmt.execute("SET profiling = 1;");
            this.setStart(System.currentTimeMillis());
            //String name = getConn().toString();
            //System.out.println("Connectio name:"+name);
            //getStmt().execute("INSERT INTO connection_log (name,status) VALUES ('" + name + "','connect')");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void connectServDB() {
        String url = "jdbc:mysql://service.bidschart.com/dev_set_social?characterEncoding=UTF8";
        String driver = "org.gjt.mm.mysql.Driver";
        String username = "dev";
        String passwd = "@x1234";

        try {
            Class.forName(driver);
            this.setConn(DriverManager.getConnection(url, username, passwd));
            //conn.setAutoCommit(false); 
            this.setStmt(getConn().createStatement());
            stmt.execute("SET profiling = 1;");
            this.setStart(System.currentTimeMillis());
            //String name = getConn().toString();
            //System.out.println("Connectio name:"+name);
            //getStmt().execute("INSERT INTO connection_log (name,status) VALUES ('" + name + "','connect')");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void closeDB() {
        try {
            if (null != this.getRs()) {
                this.getRs().close();
            }

            if (null != this.getStmt()) {
                //String name = getConn().toString();
                this.setStop(System.currentTimeMillis());
                long estimated = getStop() - getStart();
                this.getStmt().close();
            }
            if (null != this.getConn()) {
                this.getConn().close();
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @return the conn
     */
    public Connection getConn() {
        return conn;
    }

    /**
     * @param conn the conn to set
     */
    public void setConn(Connection conn) {
        this.conn = conn;
    }

    /**
     * @return the stmt
     */
    public Statement getStmt() {
        return stmt;
    }

    /**
     * @param stmt the stmt to set
     */
    public void setStmt(Statement stmt) {
        this.stmt = stmt;
    }

    /**
     * @return the rs
     */
    public ResultSet getRs() {
        return rs;
    }

    /**
     * @param rs the rs to set
     */
    public void setRs(ResultSet rs) {
        this.rs = rs;
    }

    /**
     * @return the start
     */
    public long getStart() {
        return start;
    }

    /**
     * @param start the start to set
     */
    public void setStart(long start) {
        this.start = start;
    }

    /**
     * @return the stop
     */
    public long getStop() {
        return stop;
    }

    /**
     * @param stop the stop to set
     */
    public void setStop(long stop) {
        this.stop = stop;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the driver
     */
    public String getDriver() {
        return driver;
    }

    /**
     * @param driver the driver to set
     */
    public void setDriver(String driver) {
        this.driver = driver;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the passwd
     */
    public String getPasswd() {
        return passwd;
    }

    /**
     * @param passwd the passwd to set
     */
    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    /**
     * @return the query
     */
    public String getQuery() {
        return query;
    }

    /**
     * @param query the query to set
     */
    public void setQuery(String query) {
        this.query = query;
    }
}
