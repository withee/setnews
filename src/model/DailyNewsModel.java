/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import dbmodel.DBConnection;

/**
 *
 * @author mrsyrop
 */
public class DailyNewsModel extends DBConnection {

    private int id;
    private String type="";
    private String local_date;
    private String local_time;
    private String symbol;
    private String title;
    private String link;
    private String owner;
    private String create_at;
    private String update_at;

    public DailyNewsModel() {
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //System.out.println(ft);
        this.create_at = ft.format(dNow);
        this.update_at = ft.format(dNow);
    }

    public boolean save() {
        boolean success = true;
//        this.connectDB();//เครื่องตัวเอง
        this.connectServDB();//เครื่องเซริฟเวอร์
        PreparedStatement stm = null;
        String query = "INSERT IGNORE INTO `daily_news` "
                + "(`local_date`, "
                + "`type`, "
                + "`local_time`, "
                + "`symbol`, "
                + "`title`, "
                + "`link`, "
                + "`owner`, "
                + "`create_at`, "
                + "`update_at`) "
                + "VALUES "
                + "(?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?);";

        try {
            stm = this.getConn().prepareStatement(query);
            stm.setString(1, this.getLocal_date());
            stm.setString(2, this.getType());
            stm.setString(3, this.getLocal_time());
            stm.setString(4, this.getSymbol());
            stm.setString(5, this.getTitle());
            stm.setString(6, this.getLink());
            stm.setString(7, this.getOwner());
            stm.setString(8, this.getCreate_at());
            stm.setString(9, this.getUpdate_at());
//            System.out.println(stm.toString());
            success = stm.execute();

        } catch (SQLException ex) {
            Logger.getLogger(DailyNewsModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }

        return success;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the date_time
     */
    /**
     * @return the symbol
     */
    public String getSymbol() {
        return symbol;
    }

    /**
     * @param symbol the symbol to set
     */
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link the link to set
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * @return the create_at
     */
    public String getCreate_at() {
        return create_at;
    }

    /**
     * @param create_at the create_at to set
     */
    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    /**
     * @return the update_at
     */
    public String getUpdate_at() {
        return update_at;
    }

    /**
     * @param update_at the update_at to set
     */
    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the local_date
     */
    public String getLocal_date() {
        return local_date;
    }

    /**
     * @param local_date the local_date to set
     */
    public void setLocal_date(String local_date) {
        this.local_date = local_date;
    }

    /**
     * @return the local_time
     */
    public String getLocal_time() {
        return local_time;
    }

    /**
     * @param local_time the local_time to set
     */
    public void setLocal_time(String local_time) {
        this.local_time = local_time;
    }

    /**
     * @return the owner
     */
    public String getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

}
