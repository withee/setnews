/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dbmodel.DBConnection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mrsyrop
 */
public class RssFeed extends DBConnection {

    private int rss_id;
    private String title;
    private String content;
    private String link;
    private String published_at;
    private String author;
    private String created_at;
    private String updated_at;
    private int comment_count;
    private String lass_accessed_at;
    private String provider;
    private String deleted_at;
    private String image;

    public RssFeed() {
        this.rss_id = 0;
        this.comment_count = 0;
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.created_at = sdf.format(date);
        this.updated_at = sdf.format(date);
    }

    public boolean save() {
        boolean success = false;
        //this.connectDB();
        this.connectServDB();
        PreparedStatement stm = null;
        try {
            String dupsql = "SELECT `id` FROM `rss_feeds` WHERE `link`=? limit 1";
            stm = this.getConn().prepareStatement(dupsql);
            stm.setString(1, this.getLink());
            this.rs = stm.executeQuery();
            if (!this.rs.next()) {
                String sql = "INSERT INTO `rss_feeds` "
                        + "(`rss_id`, "
                        + "`title`, "
                        + "`content`, "
                        + "`link`, "
                        + "`published_at`, "
                        + "`author`, "
                        + "`created_at`, "
                        + "`updated_at`, "
                        + "`comment_count`, "
                        + "`last_accessed_at`, "
                        + "`provider`, "
                        + "`deleted_at`, "
                        + "`image`) "
                        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
                stm = this.getConn().prepareStatement(sql);
                stm.setInt(1, this.getRss_id());
                stm.setString(2, this.getTitle());
                stm.setString(3, this.getContent());
                stm.setString(4, this.getLink());
                stm.setString(5, this.getPublished_at());
                stm.setString(6, this.getAuthor());
                stm.setString(7, this.getCreated_at());
                stm.setString(8, this.getUpdated_at());
                stm.setInt(9, this.getComment_count());
                stm.setString(10, this.getLass_accessed_at());
                stm.setString(11, this.getProvider());
                stm.setString(12, this.getDeleted_at());
                stm.setString(13, this.getImage());
                if (0 != stm.executeUpdate()) {
                    success = true;
                }
            } else {
                success = false;
            }
        } catch (SQLException ex) {
            System.out.println(stm.toString());
            Logger.getLogger(RssFeed.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (null != stm) {
                    stm.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(RssFeed.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                this.closeDB();
            }
        }
        return success;
    }

    /**
     * @return the rss_id
     */
    public int getRss_id() {
        return rss_id;
    }

    /**
     * @param rss_id the rss_id to set
     */
    public void setRss_id(int rss_id) {
        this.rss_id = rss_id;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link the link to set
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * @return the published_at
     */
    public String getPublished_at() {
        return published_at;
    }

    /**
     * @param published_at the published_at to set
     */
    public void setPublished_at(String published_at) {
        this.published_at = published_at;
    }

    /**
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @param author the author to set
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * @return the created_at
     */
    public String getCreated_at() {
        return created_at;
    }

    /**
     * @param created_at the created_at to set
     */
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    /**
     * @return the updated_at
     */
    public String getUpdated_at() {
        return updated_at;
    }

    /**
     * @param updated_at the updated_at to set
     */
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    /**
     * @return the comment_count
     */
    public int getComment_count() {
        return comment_count;
    }

    /**
     * @param comment_count the comment_count to set
     */
    public void setComment_count(int comment_count) {
        this.comment_count = comment_count;
    }

    /**
     * @return the lass_accessed_at
     */
    public String getLass_accessed_at() {
        return lass_accessed_at;
    }

    /**
     * @param lass_accessed_at the lass_accessed_at to set
     */
    public void setLass_accessed_at(String lass_accessed_at) {
        this.lass_accessed_at = lass_accessed_at;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
     * @return the deleted_at
     */
    public String getDeleted_at() {
        return deleted_at;
    }

    /**
     * @param deleted_at the deleted_at to set
     */
    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    /**
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(String image) {
        this.image = image;
    }

}
