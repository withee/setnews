/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dbmodel.DBConnection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sootipong
 */
public class WarrantModel extends DBConnection {

    private String symbol="";
    private String sp="";
    private double warrant=0;
    private double Underlying=0;
    private String Last_Exercise_date;
    private double Exercise_Ratio=0;
    private double Exercise_Price=0;
    private double Volatility=0;
    private double BlackScholes=0;
    private double Premium=0;
    private double All_In_Premium=0;
    private double Gearing=0;
    private double Delta=0;
    private double DeltaElasticity=0;
    private double Mthavgdaily=0;
    private double Listed_Warrant=0;
    private String ExercisePeriod="";
//    private String create_at;
//    private String update_at;

//    public WarrantModel() {
//        Date dNow = new Date();
//        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        //System.out.println(ft);
//        this.create_at = ft.format(dNow);
//        this.update_at = ft.format(dNow);
//    }
    public boolean save() {
        boolean success = true;
        this.connectDB();//เครื่องตัวเอง
//        this.connectServDB();//เครื่องเซริฟเวอร์
        PreparedStatement stm = null;
        String query = "INSERT IGNORE INTO `warrant` "
                + "(`symbol`, "
                + "`sp`, "
                + "`warrant`, "
                + "`Underlying`, "
                + "`Last_Exercise_date`, "
                + "`Exercise_Ratio`, "
                + "`Exercise_Price`, "
                + "`Volatility`, "
                + "`BlackScholes`, "
                + "`Premium`, "
                + "`All_In_Premium`, "
                + "`Gearing`, "
                + "`Delta`, "
                + "`DeltaElasticity`, "
                + "`1_Mth_avg_daily`, "
                + "`Listed_Warrant`, "
                + "`ExercisePeriod`) "
                + "VALUES "
                + "(?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?);";

        try {
            stm = this.getConn().prepareStatement(query);
            stm.setString(1, this.getSymbol());
            stm.setString(2, this.getSp());
            stm.setDouble(3, this.getWarrant());
            stm.setDouble(4, this.getUnderlying());
            stm.setString(5, this.getLast_Exercise_date());
            stm.setDouble(6, this.getExercise_Ratio());
            stm.setDouble(7, this.getExercise_Price());
            stm.setDouble(8, this.getVolatility());
            stm.setDouble(9, this.getBlackScholes());
            stm.setDouble(10, this.getPremium());
            stm.setDouble(11, this.getAll_In_Premium());
            stm.setDouble(12, this.getGearing());
            stm.setDouble(13, this.getDelta());
            stm.setDouble(14, this.getDeltaElasticity());
            stm.setDouble(15, this.getMthavgdaily());
            stm.setDouble(16, this.getListed_Warrant());
            stm.setString(17, this.getExercisePeriod());
//            System.out.println(stm.toString());
            success = stm.execute();

        } catch (SQLException ex) {
            Logger.getLogger(DailyNewsModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }

        return success;
    }

    /**
     * @return the symbol
     */
    public String getSymbol() {
        return symbol;
    }

    /**
     * @param symbol the symbol to set
     */
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    /**
     * @return the sp
     */
    public String getSp() {
        return sp;
    }

    /**
     * @param sp the sp to set
     */
    public void setSp(String sp) {
        this.sp = sp;
    }

    /**
     * @return the warrant
     */
    public double getWarrant() {
        return warrant;
    }

    /**
     * @param warrant the warrant to set
     */
    public void setWarrant(Float warrant) {
        this.warrant = warrant;
    }

    /**
     * @return the Underlying
     */
    public double getUnderlying() {
        return Underlying;
    }

    /**
     * @param Underlying the Underlying to set
     */
    public void setUnderlying(Float Underlying) {
        this.Underlying = Underlying;
    }

    /**
     * @return the Last_Exercise_date
     */
    public String getLast_Exercise_date() {
        return Last_Exercise_date;
    }

    /**
     * @param Last_Exercise_date the Last_Exercise_date to set
     */
    public void setLast_Exercise_date(String Last_Exercise_date) {
        this.Last_Exercise_date = Last_Exercise_date;
    }

    /**
     * @return the Exercise_Ratio
     */
    public double getExercise_Ratio() {
        return Exercise_Ratio;
    }

    /**
     * @param Exercise_Ratio the Exercise_Ratio to set
     */
    public void setExercise_Ratio(Float Exercise_Ratio) {
        this.Exercise_Ratio = Exercise_Ratio;
    }

    /**
     * @return the Exercise_Price
     */
    public double getExercise_Price() {
        return Exercise_Price;
    }

    /**
     * @param Exercise_Price the Exercise_Price to set
     */
    public void setExercise_Price(Float Exercise_Price) {
        this.Exercise_Price = Exercise_Price;
    }

    /**
     * @return the Volatility
     */
    public double getVolatility() {
        return Volatility;
    }

    /**
     * @param Volatility the Volatility to set
     */
    public void setVolatility(Float Volatility) {
        this.Volatility = Volatility;
    }

    /**
     * @return the BlackScholes
     */
    public double getBlackScholes() {
        return BlackScholes;
    }

    /**
     * @param BlackScholes the BlackScholes to set
     */
    public void setBlackScholes(Float BlackScholes) {
        this.BlackScholes = BlackScholes;
    }

    /**
     * @return the Premium
     */
    public double getPremium() {
        return Premium;
    }

    /**
     * @param Premium the Premium to set
     */
    public void setPremium(Float Premium) {
        this.Premium = Premium;
    }

    /**
     * @return the All_In_Premium
     */
    public double getAll_In_Premium() {
        return All_In_Premium;
    }

    /**
     * @param All_In_Premium the All_In_Premium to set
     */
    public void setAll_In_Premium(Float All_In_Premium) {
        this.All_In_Premium = All_In_Premium;
    }

    /**
     * @return the Gearing
     */
    public double getGearing() {
        return Gearing;
    }

    /**
     * @param Gearing the Gearing to set
     */
    public void setGearing(Float Gearing) {
        this.Gearing = Gearing;
    }

    /**
     * @return the Delta
     */
    public double getDelta() {
        return Delta;
    }

    /**
     * @param Delta the Delta to set
     */
    public void setDelta(Float Delta) {
        this.Delta = Delta;
    }

    /**
     * @return the DeltaElasticity
     */
    public double getDeltaElasticity() {
        return DeltaElasticity;
    }

    /**
     * @param DeltaElasticity the DeltaElasticity to set
     */
    public void setDeltaElasticity(Float DeltaElasticity) {
        this.DeltaElasticity = DeltaElasticity;
    }

    /**
     * @return the Mthavgdaily
     */
    public double getMthavgdaily() {
        return Mthavgdaily;
    }

    /**
     * @param Mthavgdaily the Mthavgdaily to set
     */
    public void setMthavgdaily(Float Mthavgdaily) {
        this.Mthavgdaily = Mthavgdaily;
    }

    /**
     * @return the Listed_Warrant
     */
    public double getListed_Warrant() {
        return Listed_Warrant;
    }

    /**
     * @param Listed_Warrant the Listed_Warrant to set
     */
    public void setListed_Warrant(Float Listed_Warrant) {
        this.Listed_Warrant = Listed_Warrant;
    }

    /**
     * @return the ExercisePeriod
     */
    public String getExercisePeriod() {
        return ExercisePeriod;
    }

    /**
     * @param ExercisePeriod the ExercisePeriod to set
     */
    public void setExercisePeriod(String ExercisePeriod) {
        this.ExercisePeriod = ExercisePeriod;
    }
}
