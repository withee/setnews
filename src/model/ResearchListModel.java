/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import dbmodel.DBConnection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sootipong
 */
public class ResearchListModel extends DBConnection {
   private int id;
   private String local_date;
   private String local_time;
   private String type;
   private String source;
   private String research_type;
   private String symbol;
   private String title;
   private String link;
   private String owner;
   private String create_at;
   private String update_at;
   
   public ResearchListModel() {
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //System.out.println(ft);
        this.create_at = ft.format(dNow);
        this.update_at = ft.format(dNow);
    }

    public boolean save() {
        boolean success = true;
//        this.connectDB();//เครื่องตัวเอง
        this.connectServDB();//เครื่องเซริฟเวอร์
        PreparedStatement stm = null;
        String query = "INSERT IGNORE INTO `research_list` "
                + "(`local_date`, "
                + "`local_time`, "
                + "`type`, "
                + "`source`, "
                + "`research_type`, "
                + "`symbol`, "
                + "`title`, "
                + "`link`, "
                + "`owner`, "
                + "`create_at`, "
                + "`update_at`) "
                + "VALUES "
                + "(?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?, "
                + "?);";

        try {
            stm = this.getConn().prepareStatement(query);
            stm.setString(1, this.getlocal_date());
            stm.setString(2, this.getlocal_time());
            stm.setString(3, this.getType());
            stm.setString(4, this.getSource());
            stm.setString(5, this.getResearch_type());
            stm.setString(6, this.getSymbol());
            stm.setString(7, this.getTitle());
            stm.setString(8, this.getLink());
            stm.setString(9, this.getOwner());
            stm.setString(10, this.getCreate_at());
            stm.setString(11, this.getUpdate_at());
//            System.out.println(stm.toString());
            success = stm.execute();

        } catch (SQLException ex) {
            Logger.getLogger(DailyNewsModel.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.closeDB();
        }

        return success;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    
    /**
     * @return the owner
     */
    public String getOwner() {
        return owner;
    }

    /**
     * @param owner the local_datetime to set
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     * @return the local_datetime
     */
    public String getlocal_date() {
        return local_date;
    }

    /**
     * @param local_date the local_datetime to set
     */
    public void setlocal_date(String local_date) {
        this.local_date = local_date;
    }
    
    /**
     * @return the local_datetime
     */
    public String getlocal_time() {
        return local_time;
    }

    /**
     * @param local_time the local_datetime to set
     */
    public void setlocal_time(String local_time) {
        this.local_time = local_time;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * @return the research_type
     */
    public String getResearch_type() {
        return research_type;
    }

    /**
     * @param research_type the research_type to set
     */
    public void setResearch_type(String research_type) {
        this.research_type = research_type;
    }

    /**
     * @return the symbol
     */
    public String getSymbol() {
        return symbol;
    }

    /**
     * @param symbol the symbol to set
     */
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link the link to set
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * @return the create_at
     */
    public String getCreate_at() {
        return create_at;
    }

    /**
     * @param create_at the create_at to set
     */
    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    /**
     * @return the update_at
     */
    public String getUpdate_at() {
        return update_at;
    }

    /**
     * @param update_at the update_at to set
     */
    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }
}
