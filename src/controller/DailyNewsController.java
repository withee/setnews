/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DailyNewsModel;
import model.ResearchListModel;
import model.RssFeed;
import model.WarrantModel;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

/**
 *
 * @author mrsyrop
 */
public class DailyNewsController {

    private String userAgent = "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36";

    //แปลงจากวันที่ที่เป็น 30 ม.ค. 2558 เป็น 2558-01-30
    public static String editDMYE(String str) {
        String[] arr_str = str.split("-");
        if (arr_str.length >= 3) {
            String m = arr_str[1];
            String month = (m.equals("Jan")) ? "01"
                    : (m.equals("Feb")) ? "02"
                    : (m.equals("Mar")) ? "03"
                    : (m.equals("Apr")) ? "04"
                    : (m.equals("May")) ? "05"
                    : (m.equals("Jun")) ? "06"
                    : (m.equals("Jul")) ? "07"
                    : (m.equals("Aug")) ? "08"
                    : (m.equals("Sep")) ? "09"
                    : (m.equals("Oct")) ? "10"
                    : (m.equals("Nov")) ? "11"
                    : "12";
            return (Integer.parseInt(arr_str[2])) + "-" + month + "-" + arr_str[0];
        } else {
            return "-";
        }
    }

    //แปลงจากวันที่ที่เป็น 30 ม.ค. 2558 เป็น 2558-01-30
    public static String editDMY(String str) {
        if (str.equals("-") || str.equals("")) {
            return "-";
        } else {
            String[] arr_str = str.split(" ");
//            System.out.println(arr_str.length);
            if (arr_str.length >= 3) {
                String m = arr_str[1];
                String month = (m.equals("ม.ค.")) ? "01"
                        : (m.equals("ก.พ.")) ? "02"
                        : (m.equals("มี.ค.")) ? "03"
                        : (m.equals("เม.ย.")) ? "04"
                        : (m.equals("พ.ค.")) ? "05"
                        : (m.equals("มิ.ย.")) ? "06"
                        : (m.equals("ก.ค.")) ? "07"
                        : (m.equals("ส.ค.")) ? "08"
                        : (m.equals("ก.ย.")) ? "09"
                        : (m.equals("ต.ค.")) ? "10"
                        : (m.equals("พ.ย.")) ? "11"
                        : "12";
                return (Integer.parseInt(arr_str[2]) - 543) + "-" + month + "-" + arr_str[0];
            } else {
                return "-";
            }
        }
    }

    public void getNewsList(String companyNews, String exchangeNews, String newsType, String from, String to) {
        String company = "false";
        String exchange = "false";
        if (companyNews.equals("on")) {
            company = "true";
        }
        if (exchangeNews.equals("on")) {
            exchange = "true";
        }

        String url = "http://www.set.or.th/set/newslist.do?"
                + "company=" + company
                + "&companyNews=" + companyNews
                + "&exchange=" + exchange
                + "&exchangeNews=" + exchangeNews
                + "&symbol="
                + "&exchangeSymbols="
                + "&newsType=" + newsType
                + "&headline="
                + "&from=" + from
                + "&to=" + to
                + "&submit=ค้นหา"
                + "&language=th"
                + "&country=TH";
//        System.out.println(url);
        try {
            Document doc = Jsoup.connect(url).get();
            //        System.out.println(doc.toString());
            Elements table_res = doc.select(".table-info-wrap");
            for (Element table : table_res) {
                Element tbody = table.select("tbody").first();
                //System.out.println(tbody.toString());                
                for (Element tr : tbody.children()) {
//                    System.out.println("-------------*********----------------");
                    boolean have_data = true;
                    DailyNewsModel dailynews = new DailyNewsModel();
                    dailynews.setType(newsType);
                    dailynews.setOwner("www.set.or.th");
                    int td_index = 1;
                    for (Element td : tr.children()) {
                        switch (td_index) {
                            case 1:
                                String date = "";
                                String[] arr_str = td.text().trim().split(" ");
                                String month = (arr_str[1].equals("ม.ค.")) ? "01"
                                        : (arr_str[1].equals("ก.พ.")) ? "02"
                                        : (arr_str[1].equals("มี.ค.")) ? "03"
                                        : (arr_str[1].equals("เม.ย.")) ? "04"
                                        : (arr_str[1].equals("พ.ค.")) ? "05"
                                        : (arr_str[1].equals("มิ.ย.")) ? "06"
                                        : (arr_str[1].equals("ก.ค.")) ? "07"
                                        : (arr_str[1].equals("ส.ค.")) ? "08"
                                        : (arr_str[1].equals("ก.ย.")) ? "09"
                                        : (arr_str[1].equals("ต.ค.")) ? "10"
                                        : (arr_str[1].equals("พ.ย.")) ? "11"
                                        : "12";
                                date += (Integer.parseInt(arr_str[2]) - 543) + "-" + month + "-" + arr_str[0];
                                dailynews.setLocal_date(date);
                                dailynews.setLocal_time(arr_str[3]);
                                break;
                            case 3:
                                dailynews.setSymbol(td.text().trim());
                                break;
                            case 4:
                                dailynews.setTitle(td.text().trim());
                                break;
                            case 5:
                                dailynews.setLink(td.children().first().attr("href"));
                                break;
                            default:
                                break;
                        }
                        td_index++;
                    }
                    dailynews.save();
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(DailyNewsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void getNewsSettrade() {
//        select_period จำนวนวัน ย้อนหลัง
        String url = "http://www.settrade.com/C17_ResearchList.jsp?"
                + "keyword="
                + "&mode"
                + "&view"
                + "&page=1"
                + "&security_name"
                + "&select_broker=all"
                + "&select_category=all"
                + "&select_period=1"
                + "&select_security_type=all"
                + "&select_sub_all=all"
                + "&select_sub_bex=21"
                + "&select_sub_mf=22"
                + "&select_sub_other=all"
                + "&select_sub_set=all"
                + "&select_sub_tfex=all"
                + "&txtBrokerId=IPO"
                + "&view=published";

        try {
            Document doc = Jsoup.connect(url).get();
//            Elements table_res = doc.select(".table-info");
            Elements div_colorGreen = doc.select(".colorGreen");
            for (Element div : div_colorGreen) {
//                System.out.println(div.attr("class").toString());
                if (div.attr("class").toString().equals("col-xs-6 text-right colorGreen")) {
                    String str = div.text().trim();
                    String[] arr_str = str.split(" ");
                    float countList = Float.parseFloat(arr_str[5]);
                    int page = (int) Math.ceil(countList / 50);
                    for (int i = 1; i <= page; i++) {
                        String urlPgae = "http://www.settrade.com/C17_ResearchList.jsp?"
                                + "keyword="
                                + "&mode"
                                + "&view"
                                + "&page=" + i
                                + "&security_name"
                                + "&select_broker=all"
                                + "&select_category=all"
                                + "&select_period=1"
                                + "&select_security_type=all"
                                + "&select_sub_all=all"
                                + "&select_sub_bex=21"
                                + "&select_sub_mf=22"
                                + "&select_sub_other=all"
                                + "&select_sub_set=all"
                                + "&select_sub_tfex=all"
                                + "&txtBrokerId=IPO"
                                + "&view=published";
//                        System.out.println(urlPgae);
                        Document docPage = Jsoup.connect(urlPgae).get();
                        Elements table_res = docPage.select(".table-info");
                        for (Element table : table_res) {
                            Element tbody = table.select("tbody").first();
                            for (Element tr : tbody.children()) {
                                ResearchListModel researchList = new ResearchListModel();
                                researchList.setOwner("www.settrade.com");
                                int td_index = 1;
                                for (Element td : tr.children()) {
                                    switch (td_index) {
                                        case 1:
                                            String str_date = "";
                                            String[] arr_dateTime = td.text().split(" - ");
                                            String[] arr_date = arr_dateTime[0].split("/");
                                            str_date += "20" + arr_date[2];
                                            str_date += "-" + arr_date[1];
                                            str_date += "-" + arr_date[0];

                                            researchList.setlocal_date(str_date);
                                            researchList.setlocal_time(arr_dateTime[1]);
                                            break;
                                        case 2:
//                                            System.out.println(td.text().trim());
                                            researchList.setType(td.text().trim());
                                            break;
                                        case 3:
//                                            System.out.println(td.text().trim());
                                            researchList.setSource(td.text().trim());
                                            break;
                                        case 4:
//                                            System.out.println(td.text().trim());
                                            researchList.setResearch_type(td.text().trim());
                                            break;
                                        case 5:
//                                            System.out.println(td.text().trim());
                                            researchList.setSymbol(td.text().trim());
                                            break;
                                        case 6:
//                                            System.out.println(td.children().first().attr("href"));
                                            researchList.setTitle(td.text().trim());
                                            researchList.setLink(td.children().first().attr("href"));
                                            break;
                                        default:
                                            break;
                                    }
                                    td_index++;
                                }
                                researchList.save();
                            }
                        }
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(DailyNewsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Path download(String sourceUrl, String targetDirectory) throws MalformedURLException, IOException {
        URL url = new URL(sourceUrl);

        String fileName = url.getFile();

        Path targetPath = new File(targetDirectory + fileName).toPath();

        Files.copy(url.openStream(), targetPath, StandardCopyOption.REPLACE_EXISTING);

        return targetPath;
    }

    public void readFileExcelForUrl() throws MalformedURLException, IOException {
        URL u = new URL("http://image.bangkokbiznews.com/kt/media/file/news/2016/04/26/696001/Warrant-26.xls");
        URLConnection uc = u.openConnection();

        String encoding = new sun.misc.BASE64Encoder().encode("username:password".getBytes());
        uc.setRequestProperty("Authorization", "Basic " + encoding);

        String contentType = uc.getContentType();
        int contentLength = uc.getContentLength();
        if (contentType.startsWith("text/") || contentLength == -1) {
            throw new IOException("This is not a binary file.");
        }

        //System.out.println(uc.getContentEncoding());
        //System.out.println(contentLength);
        InputStream raw = uc.getInputStream();
        InputStream in = new BufferedInputStream(raw);
        byte[] data = new byte[contentLength];
        int bytesRead = 0;
        int offset = 0;
        while (offset < contentLength) {
            bytesRead = in.read(data, offset, data.length - offset);
            if (bytesRead == -1) {
                break;
            }
            offset += bytesRead;
        }
        in.close();

        if (offset != contentLength) {
            throw new IOException("Only read " + offset + " bytes; Expected " + contentLength + " bytes");
        }

        FileOutputStream out = new FileOutputStream("E:\\project\\setnews\\Warrant.xls");
        out.write(data);
        out.flush();
        out.close();
    }

    public void readFileExcel() throws IOException {
//        this.readFileExcelForUrl();
        String filePathString = "E:/project/setnews/Warrant.xls";
        File f = new File(filePathString);
        if (f.exists() && !f.isDirectory()) {
            try {
                POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(filePathString));
                HSSFWorkbook wb = new HSSFWorkbook(fs);
                HSSFSheet sheet = wb.getSheetAt(0);
                HSSFRow row;
                HSSFCell cell;

                int rows; // No of rows
                rows = sheet.getPhysicalNumberOfRows();

                int cols = 0; // No of columns
                int tmp = 0;

                // This trick ensures that we get the data properly even if it doesn't start from first few rows
                for (int i = 0; i < 10 || i < rows; i++) {
                    row = sheet.getRow(i);
                    if (row != null) {
                        tmp = sheet.getRow(i).getPhysicalNumberOfCells();
                        if (tmp > cols) {
                            cols = tmp;
                        }
                    }
                }
                //rows=จำนวนแถวหมด
                for (int r = 5; r < (151); r++) {
                    row = sheet.getRow(r);
                    if (row != null) {
                        WarrantModel warrant = new WarrantModel();
                        for (int c = 0; c < cols; c++) {
                            cell = row.getCell((short) c);
                            if (cell != null) {
//                                if(cell.getCellType()==0){
//                                    System.out.println("number : "+r+" : "+c+" : "+cell.getNumericCellValue());
//                                }else{
//                                    System.out.println("text : "+r+" : "+c+" : "+cell.getStringCellValue());
//                                }
//                                
//                                if(c==4){
//                                    System.out.println("number : "+r+" : "+c+" : "+editDMYE(cell.toString()));
//                                }

                                if (cell.getCellType() == 1 && (cell.getStringCellValue().equals("na") || cell.getStringCellValue().equals("nm"))) {
//                                    System.out.println("not Set");        
                                } else {
//                                    System.out.println("Set");
                                    switch (c) {
                                        case 0:
                                            warrant.setSymbol(cell.getStringCellValue());
                                            break;
                                        case 1:
                                            warrant.setSp(cell.getStringCellValue());
                                            break;
                                        case 2:
                                            warrant.setWarrant((float) cell.getNumericCellValue());
                                            break;
                                        case 3:
                                            warrant.setUnderlying((float) cell.getNumericCellValue());
                                            break;
                                        case 4:
                                            warrant.setLast_Exercise_date(editDMYE(cell.toString()));
                                            break;
                                        case 5:
                                            warrant.setExercise_Ratio((float) cell.getNumericCellValue());
                                            break;
                                        case 6:
                                            warrant.setExercise_Price((float) cell.getNumericCellValue());
                                            break;
                                        case 7:
                                            warrant.setVolatility((float) cell.getNumericCellValue());
                                            break;
                                        case 8:
                                            warrant.setBlackScholes((float) (cell.getNumericCellValue() * 100));
                                            break;
                                        case 9:
                                            warrant.setPremium((float) (cell.getNumericCellValue() * 100));
                                            break;
                                        case 10:
                                            warrant.setAll_In_Premium((float) cell.getNumericCellValue());
                                            break;
                                        case 11:
                                            warrant.setGearing((float) cell.getNumericCellValue());
                                            break;
                                        case 12:
                                            warrant.setDelta((float) cell.getNumericCellValue());
                                            break;
                                        case 13:
                                            warrant.setDeltaElasticity((float) cell.getNumericCellValue());
                                            break;
                                        case 14:
                                            warrant.setMthavgdaily((float) cell.getNumericCellValue());
                                            break;
                                        case 15:
                                            warrant.setListed_Warrant((float) cell.getNumericCellValue());
                                            break;
                                        case 16:
                                            warrant.setExercisePeriod(cell.getStringCellValue());
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                        warrant.save();
                    }
                }
            } catch (Exception ioe) {
                ioe.printStackTrace();
            }
        } else {
            System.out.println("Flase");
        }
    }

    public void getNews() {
        String url = "http://www.set.or.th/set/todaynews.do?period=all&language=th&country=TH&market=";

        try {
            Document doc = Jsoup.connect(url).get();
            //System.out.println(doc.toString());

            Element localdate = doc.select(".text-right").last();

//            System.out.println(editDMY(localdate.text()));
            //for(Element local:localdate){
            //    System.out.println(local.toString());
            //}
            Elements table_res = doc.select(".table-responsive");
            for (Element table : table_res) {
                //System.out.println(table.toString());
                Element tbody = table.select("tbody").first();
                //System.out.println(tbody.toString());                
                for (Element tr : tbody.children()) {
//                    System.out.println(localdate.text().trim());
                    boolean have_data = true;
                    DailyNewsModel dailynews = new DailyNewsModel();
                    dailynews.setLocal_date(editDMY(localdate.text().trim()));
                    dailynews.setOwner("www.set.or.th");
//                    dailynews.setType("");
                    int td_index = 1;
                    for (Element td : tr.children()) {
                        //System.out.println(td.toString());
                        if (!td.hasAttr("colspan")) {
                            //System.out.println(td.toString());
//                            dailynews.setLocal_date(url);
                            switch (td_index) {
                                case 1:
                                    dailynews.setLocal_time(td.text().trim());
                                    break;
                                case 3:
                                    dailynews.setSymbol(td.text().trim());
                                    break;
                                case 4:
                                    dailynews.setTitle(td.text().trim());
                                    break;
                                case 5:
                                    //System.out.println(td.children().first().attr("href"));
                                    dailynews.setLink(td.children().first().attr("href"));
                                    break;
                                default:
                                    break;

                            }

                        } else {
                            have_data = false;
                        }
                        td_index++;
                    }
                    if (have_data) {
//                        System.out.println(dailynews.getLocal_date()+" :: "+dailynews.getLocal_time());
                        boolean status = dailynews.save();
//                        System.out.println(dailynews.getLocal_date()+" :: "+dailynews.getLocal_time()+" :: "+dailynews.getSymbol()+" :: "+status);
                    }
                }

            }

        } catch (IOException ex) {
            Logger.getLogger(DailyNewsController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void getthansettakij() {
        String baseurl = "http://www.thansettakij.com/category/business/financial-investment";

        try {
            Document document = Jsoup.connect(baseurl).userAgent(userAgent).get();
            String lastpage = document.select("a.last").first().text();
            Element category = document.select("div.td-category-grid").first();
            Elements link = category.select("a[rel=\"bookmark\"]");
            for (Element a : link) {
                //System.out.println(a.toString());
                if (!a.children().isEmpty()) {
                    RssFeed rss = new RssFeed();
                    rss.setLink(a.attr("href"));
                    rss.setTitle(a.attr("title"));
                    rss.setImage(a.child(0).attr("src"));
                    rss.setProvider("thansettakij.com");
                    rss.setAuthor("ฐานเศรษฐกิจ");
                    this.thansettakijContent(rss);
                    if (!rss.save()) {
                        System.out.println("Duplicate link:" + rss.getLink());
                        break;
                    }
                }
            }

            boolean duplicatelink = false;
            for (int i = 1; i <= Integer.parseInt(lastpage); i++) {
                System.out.println("PAGE: " + i);
                String url = "http://www.thansettakij.com/category/business/financial-investment/page/" + i;
                Document doc = Jsoup.connect(url).userAgent(userAgent).get();

                Element content = doc.select("div.td-main-content-wrap").first();
                Element maincontent = doc.select("div.td-main-content").first();
                link = maincontent.select("a[rel=\"bookmark\"]");
                for (Element a : link) {
                    if (!a.children().isEmpty()) {
                        RssFeed rss = new RssFeed();
                        rss.setLink(a.attr("href"));
                        rss.setTitle(a.attr("title"));
                        rss.setImage(a.child(0).attr("src"));
                        rss.setProvider("thansettakij.com");
                        rss.setAuthor("ฐานเศรษฐกิจ");
                        this.thansettakijContent(rss);
                        if (!rss.save()) {
                            System.out.println("Duplicate link:" + rss.getLink());
                            duplicatelink = true;
                            break;
                        }
                    }
                }
                if (duplicatelink) {
                    break;
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(DailyNewsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void thansettakijContent(RssFeed rss) {
        try {
            Document contentdoc = Jsoup.connect(rss.getLink()).userAgent(userAgent).get();
            Element header = contentdoc.select("div.td-module-meta-info").first();
            //System.out.println(header.toString());
            Element publish = header.select(".entry-date").first();
            String[] date = publish.attr("datetime").split("\\+");
            rss.setPublished_at(date[0].replaceAll("T", " "));
            Element author = header.select(".td-post-author-name").first();
            rss.setAuthor(author.child(0).text());
            Element content = contentdoc.select(".td-post-content").first();
            rss.setContent(content.text());
        } catch (IOException ex) {
            Logger.getLogger(DailyNewsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void getBangkokbiznews() {
        String baseurl = "http://www.bangkokbiznews.com/finance/list/1";
        boolean hasnext = true;
        try {
            while (hasnext) {
                Document doc = Jsoup.connect(baseurl).userAgent(userAgent).get();
                //System.out.println(doc.toString());
                Element contianer = doc.select("div.list-container").first();
                Elements articles = contianer.select("article");
                for (Element article : articles) {
                    RssFeed rss = new RssFeed();
                    rss.setProvider("bangkokbiznews.com");
                    rss.setAuthor("กรุงเทพธุรกิจออนไลน์");
                    Element link = article.select("a").first();
                    rss.setLink(link.attr("href"));
                    Element img = article.select("img").first();
                    rss.setImage(img.attr("src"));
                    rss.setTitle(img.attr("alt"));
                    this.bangkokbiznewsContent(rss);
                    if (!rss.save()) {
                        hasnext = false;
                        break;
                    }
                }

                Element next = doc.select("span.next").first();
                if (next == null) {
                    hasnext = false;
                } else {
                    baseurl = next.child(0).attr("href");
                    System.out.println(next.child(0).attr("href"));
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(DailyNewsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void bangkokbiznewsContent(RssFeed rss) {
        try {
            Document doc = Jsoup.connect(rss.getLink()).userAgent(userAgent).get();
            Element entry = doc.select("div.content-entry").first();
            Element author = entry.select("span[itemprop=author]").first();
            rss.setAuthor(author.text());
            Element date = entry.select("time").first();
            String[] dt = date.attr("datetime").split(" \\+");
            rss.setPublished_at(dt[0]);
            Element body = doc.select("div[itemprop=articlebody]").first();
            Element article = body.select("article").first();
            rss.setContent(article.text());
        } catch (IOException ex) {
            Logger.getLogger(DailyNewsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void previousnews(){
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
        int i= -1;
        int count=0;
        String dt = "";
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, i);
        dt = ft.format(c.getTime());
//        System.out.println(dt);
        for (int r = 0; r < (50); r++) {
            String url = "http://www.set.or.th/set/newslist.do?"
                    + "&from=" + dt
                    + "&to=" + dt
                    + "&currentpage="+r;
//            System.out.println(url);
            try {
                Document doc = Jsoup.connect(url).get();    
                Elements table_res = doc.select(".table-info-wrap");
                Elements table_info = doc.select(".table-info");
                if(table_res.size()==1){
                    for (Element table : table_res) {
                        Element tbody = table.select("tbody").first();
                        //System.out.println(tbody.toString());                
                        for (Element tr : tbody.children()) {
        //                    System.out.println("-------------*********----------------");
                            boolean have_data = true;
                            DailyNewsModel dailynews = new DailyNewsModel();
                            dailynews.setOwner("www.set.or.th");
                            int td_index = 1;
                            for (Element td : tr.children()) {
                                switch (td_index) {
                                    case 1:
                                        String date = "";
                                        String[] arr_str = td.text().trim().split(" ");
                                        String month = (arr_str[1].equals("ม.ค.")) ? "01"
                                                : (arr_str[1].equals("ก.พ.")) ? "02"
                                                : (arr_str[1].equals("มี.ค.")) ? "03"
                                                : (arr_str[1].equals("เม.ย.")) ? "04"
                                                : (arr_str[1].equals("พ.ค.")) ? "05"
                                                : (arr_str[1].equals("มิ.ย.")) ? "06"
                                                : (arr_str[1].equals("ก.ค.")) ? "07"
                                                : (arr_str[1].equals("ส.ค.")) ? "08"
                                                : (arr_str[1].equals("ก.ย.")) ? "09"
                                                : (arr_str[1].equals("ต.ค.")) ? "10"
                                                : (arr_str[1].equals("พ.ย.")) ? "11"
                                                : "12";
                                        date += (Integer.parseInt(arr_str[2]) - 543) + "-" + month + "-" + arr_str[0];
                                        dailynews.setLocal_date(date);
                                        dailynews.setLocal_time(arr_str[3]);
                                        break;
                                    case 3:
                                        dailynews.setSymbol(td.text().trim());
                                        break;
                                    case 4:
                                        dailynews.setTitle(td.text().trim());
                                        break;
                                    case 5:
                                        dailynews.setLink(td.children().first().attr("href"));
                                        break;
                                    default:
                                        break;
                                }
                                td_index++;
                            }
//                            System.out.println(dailynews.getLocal_date()+" :: "+dailynews.getLocal_time());
                            dailynews.save();
                        }
                    }
//                    System.out.println(r);
                }
//                System.out.println(table_res.size());
//                System.out.println(table_info.size());
            } catch (IOException ex) {
                 Logger.getLogger(DailyNewsController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
//        System.out.print(ft.format(dNow));
    }

}
