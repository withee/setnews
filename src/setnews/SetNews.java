/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package setnews;

import controller.DailyNewsController;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mrsyrop
 */
public class SetNews {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParseException, IOException {
        // TODO code application logic here
        //System.out.println("Hello World");
        int set = 0;
        DailyNewsController dailynewscontroller = new DailyNewsController();
        if (set == 0) {
//            while (true) {
                Date dt = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("HH");
                String h = sdf.format(dt);
                try {
                    dailynewscontroller.getNews();
                    if (Integer.parseInt(h) > 9 && Integer.parseInt(h) < 18) {
                        TimeUnit.SECONDS.sleep(30);
                    } else {
                        TimeUnit.MINUTES.sleep(1);
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(SetNews.class.getName()).log(Level.SEVERE, null, ex);
                }
//            }
        } else if (set == 2) {
            //ประเภทข้าว set.or.th
            //25 Trading Alert List
            //17 หลักทรัพย์ที่สมาชิกต้องให้ลูกค้าวางเงินสดเต็มจำนวน
            //20 ข่าวแจ้ง Investor Alert News
            //21 ข่าวแจ้ง Reprimand News
            //26 ชี้แจงข่าวหรือข้อมูล
            //19 สรุปผลการดำเนินงานของบจ./ รวมของบริษัทย่อย F45
            //23 งบการเงิน (Zip File)
            //101 การปรับสัญญาซื้อขายล่วงหน้า (CA)
            //102 การประกาศจำนวน Position Limit (PL)
            int countDay = (-1);
            String[] factorial = {"17", "19", "20", "21", "23", "26", "25", "101", "102"};
            for (int i = (-1); i >= countDay; i--) {
                for (String t : factorial) {
                    String dt = "";
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    Calendar c = Calendar.getInstance();
                    c.add(Calendar.DATE, i);
                    dt = sdf.format(c.getTime());
                    dailynewscontroller.getNewsList("on", "", t, dt, dt);
                    dailynewscontroller.getNewsList("", "on", t, dt, dt);
                }
            }
        } else if (set == 3) {
            while (true) {
                Date dt = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("HH");
                String h = sdf.format(dt);
                try {
                    dailynewscontroller.getNewsSettrade();
                    if (Integer.parseInt(h) > 9 && Integer.parseInt(h) < 18) {
                        TimeUnit.SECONDS.sleep(30);
                    } else {
                        TimeUnit.MINUTES.sleep(1);
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(SetNews.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } else if (set == 4) {
            dailynewscontroller.readFileExcel();
        } else if (set == 5) {
            dailynewscontroller.readFileExcelForUrl();
        } else if (set == 6) {
            dailynewscontroller.getthansettakij();
        } else if (set == 7) {
            dailynewscontroller.getBangkokbiznews();
        }else if(set==8){
            dailynewscontroller.previousnews();
        }
    }

}
